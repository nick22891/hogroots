<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model  {

    public $timestamps = false;

    protected $table = 'students';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fname', 'lname', 'address', 'magic', 'quidditch'];


    public function house()
    {
        return $this->belongsTo('App\House');
    }

    public function tests()
    {
        return $this->belongsToMany('App\Test')->withPivot('grade');;
    }

}
