<?php namespace App\Http\Controllers;

use App\House;
use App\Student;
use App\Test;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class StudentController extends Controller {

    function addStudent () {

        $houses = House::all();

        return view('addstudent', ['houses' => $houses]);

    }

    function saveStudent () {

        $house_id = Input::get('house');

        $house = House::find($house_id);

        $student = new Student();

        $student->fname = Input::get('fname');

        $student->lname = Input::get('lname');

        $student->address = Input::get('address');

        $student->magic = Input::get('magic');

        $student->quidditch = Input::get('quidditch');

        //$student->houses()->attach($house_id);

        $house->students()->save($student);

        $student->save();

        return Redirect::route('students');//shows all the students after saving

    }

    function getStudents () {

        $students = Student::with('house')->get();

        return view('students', ['students' => $students]);

    }

}
