<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model  {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function students()
    {
        return $this->belongsToMany('App\Student')->withPivot('grade');;
    }

}
