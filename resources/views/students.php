<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 5/8/15
 * Time: 3:38 PM
 */

?>

<html>

<head>

    <title>Add A New Student</title>

</head>

<body>

<h1>Students</h1>

<a href="addStudent">Add Student</a><br><br>

<table>

    <tr><td>Student ID</td><td>First Name</td><td>Last Name</td><td>Address</td><td>Magic or Muggle?</td><td>Quidditch?</td><td>House</td></tr>

 <?php

        foreach ($students as $student) {

            echo "<tr><td>" . $student->id . "</td><td>" . $student->fname . "</td><td>" . $student->lname . "</td><td>" . $student->address . "</td><td>" . ($student->magic ? "Magic" : "Muggle") . "</td><td>" . ($student->quidditch ? "Yes" : "No") . "</td><td>" . $student->house->name . "</td></tr>";

        }

 ?>

</table>

</body>

</html>