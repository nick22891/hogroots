<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 5/8/15
 * Time: 3:38 PM
 */

?>

<html>

<head>

    <title>Add A New Student</title>

</head>

<body>

    <h1>Add Student</h1>

    <form action="" method="post">

        First Name : <input type="text" name="fname"/><br><br>

        Last Name : <input type="text" name="lname"/><br><br>

        Address : <input type="text" name="address"/><br><br>

        Magic? : <select name="magic">

                    <option value="1">Magic</option>

                    <option value="0">Muggle</option>

                </select><br><br>

        Plays Quidditch? : <select name="quidditch">

                            <option value="1">Yes</option>

                            <option value="0">No</option>

                             </select><br><br>

        House : <select name="house"> <?php

        foreach ($houses as $house) {

            echo "<option value='" . $house->id . "'>" . $house->name . "</option>";

        }

        ?>

            </select><br><br>

        <input type="submit" value="Create Student!"/>

    </form>

</body>

</html>